package com.digicoreassessment.utils;

public class SecurityAuthorisationConstants {

    public static final long TOKEN_EXPIRATION_TIME = 864_000_000;
    public static final String[] PUBLIC_URIS = new String[]{
            "/create_account",
            "/login"
    };
}
