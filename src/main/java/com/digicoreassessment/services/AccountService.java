package com.digicoreassessment.services;

import com.digicoreassessment.models.Account;
import com.digicoreassessment.models.Transactions;
import com.digicoreassessment.payload.request.DepositRequest;
import com.digicoreassessment.payload.request.LoginRequest;
import com.digicoreassessment.payload.request.RegisterAccount;
import com.digicoreassessment.payload.request.WithdrawalRequest;
import com.digicoreassessment.payload.response.AccountInfo;
import com.digicoreassessment.payload.response.AccountResponse;
import com.digicoreassessment.payload.response.LoginResponse;
import com.digicoreassessment.security.service.UserDetailService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AccountService {
    Account findByAccountNumber(String accountNumber);

    AccountResponse createAccount(RegisterAccount registerAccount);

    LoginResponse login(LoginRequest request, AuthenticationManager authenticationManager, UserDetailService userDetailService);

    AccountInfo getAccountInfo(String accountNumber);

    List<Transactions> getAccountStatement(String accountNumber);

    AccountResponse withdrawMoney(WithdrawalRequest withdrawalRequest);

    AccountResponse depositMoney(DepositRequest depositRequest);
}
