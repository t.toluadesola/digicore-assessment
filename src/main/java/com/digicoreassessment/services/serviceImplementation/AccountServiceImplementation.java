package com.digicoreassessment.services.serviceImplementation;

import com.digicoreassessment.exceptions.BadRequestException;
import com.digicoreassessment.models.Account;
import com.digicoreassessment.models.AccountStatement;
import com.digicoreassessment.models.TransactionType;
import com.digicoreassessment.models.Transactions;
import com.digicoreassessment.payload.request.DepositRequest;
import com.digicoreassessment.payload.request.LoginRequest;
import com.digicoreassessment.payload.request.RegisterAccount;
import com.digicoreassessment.payload.request.WithdrawalRequest;
import com.digicoreassessment.payload.response.AccountInfo;
import com.digicoreassessment.payload.response.AccountResponse;
import com.digicoreassessment.payload.response.LoginResponse;
import com.digicoreassessment.security.service.UserDetailService;
import com.digicoreassessment.services.AccountService;
import com.digicoreassessment.utils.JWTUtil;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class AccountServiceImplementation implements AccountService {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final JWTUtil jwtUtil;

    List<Account> accounts;
    List<Transactions> transactions;

    @Override
    public Account findByAccountNumber(String accountNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(accountNumber)) return account;
        }
        return null;
    }

    @Override
    public AccountResponse createAccount(RegisterAccount registerAccount) {

        if(registerAccount.getInitialDeposit() < 500) {
            return new AccountResponse(
                    400,
                    false,
                    "Initial deposit must be greater than #500:00");
        }

        if(findByAccountName(registerAccount.getAccountName())){
            return new AccountResponse(
                    400,
                    false,
                    "Account name already exists!!"
            );
        }

        String str1 = String.valueOf((int)(Math.random() * 100000));
        String str2 = String.valueOf((int)(Math.random() * 100000));

        String accountNumber = str2+str1;

        Account account = new Account(
                registerAccount.getAccountName(),
                accountNumber,
                bCryptPasswordEncoder.encode(registerAccount.getAccountPassword()),
                registerAccount.getInitialDeposit()
        );

        accounts.add(account);

        return new AccountResponse(
                200,
                true,
                "An Account with Account Number " + accountNumber + " has been created successfully for " + registerAccount.getAccountName()
        );
    }

    @Override
    public LoginResponse login(LoginRequest request, AuthenticationManager authenticationManager, UserDetailService userDetailService){
        authenticationManager
                .authenticate(
                        new UsernamePasswordAuthenticationToken(
                                request.getAccountNumber(), request.getAccountPassword()
                        ));

        UserDetails userDetails = userDetailService.loadUserByUsername(request.getAccountNumber());

        String jwtToken = jwtUtil.generateToken(userDetails);

        return new LoginResponse(true, jwtToken);
//        return null;
    }

    @Override
    public AccountResponse withdrawMoney(WithdrawalRequest request){
        Account myAccount = findByAccountNumber(request.getAccountNumber());

        if(myAccount==null){
            return new AccountResponse(400, false, "Invalid account Number, " + request.getAccountNumber());
        }

        double withdrawalBalance = myAccount.getBalance() - request.getWithdrawnAmount();

        if(request.getWithdrawnAmount() < 1 || withdrawalBalance < 500){
            return new AccountResponse(400, false, "insufficient funds");
        }

        myAccount.setBalance(withdrawalBalance);

        transactions.add(
            new Transactions(
                request.getAccountNumber(),
                new AccountStatement(
                        LocalDateTime.now(),
                        TransactionType.WITHDRAWAL,
                        "Withdrew " + request.getWithdrawnAmount(),
                        request.getWithdrawnAmount(),
                        withdrawalBalance
                )
            )
        );
        return new AccountResponse(200, true, "withdrawal successful.");
    }

    @Override
    public AccountResponse depositMoney(DepositRequest request) {
        Account myAccount = findByAccountNumber(request.getAccountNumber());

        if(myAccount==null){
            return new AccountResponse(400, false, "invalid account Number, " + request.getAccountNumber());
        }
        Double myBalance = myAccount.getBalance() + request.getAmount();
        if(request.getAmount() > 1 && request.getAmount() < 1000000){
            myAccount.setBalance(myBalance);
        }else {
            return new AccountResponse(400, false, "This amount cannot be processed for this transaction");
        }

        transactions.add(
                new Transactions(
                        request.getAccountNumber(),
                        new AccountStatement(
                                LocalDateTime.now(),
                                TransactionType.DEPOSIT,
                                "Deposited " + request.getAmount(),
                                request.getAmount(),
                                myBalance
                        )
                )
        );

        return new AccountResponse(200, true, "payment successful.");

    }

    private boolean findByAccountName(String accountName) {
        for(Account account : accounts) {
            if (account.getAccountName().equals(accountName)) return true;
        }
        return false;
    }

    @Override
    public AccountInfo getAccountInfo(String accountNumber) {
        Account account = findByAccountNumber(accountNumber);

        if(account==null){
            throw new BadRequestException("Account Does not Exist");
        }

        return new AccountInfo(200, true, "successful", account);

    }

    @Override
    public List<Transactions> getAccountStatement(String accountNumber) {

        List<Transactions> result = new ArrayList<>();

        for(Transactions transaction : transactions){
            if(transaction.getAccountNumber().equals(accountNumber)){
                result.add(transaction);
            }
        }

        return result;

    }
}
