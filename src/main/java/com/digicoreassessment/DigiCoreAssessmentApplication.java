package com.digicoreassessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
public class DigiCoreAssessmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigiCoreAssessmentApplication.class, args);
    }

}
