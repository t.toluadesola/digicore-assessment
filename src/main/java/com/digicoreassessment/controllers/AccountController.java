package com.digicoreassessment.controllers;

import com.digicoreassessment.payload.request.DepositRequest;
import com.digicoreassessment.payload.request.WithdrawalRequest;
import com.digicoreassessment.services.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/account_info/{accountNumber}")
    public ResponseEntity<?> getAccountInfo(@PathVariable String accountNumber){
        return new ResponseEntity<>(accountService.getAccountInfo(accountNumber), HttpStatus.OK);
    }

    @GetMapping("/account_statement/{accountNumber}")
    public ResponseEntity<?> getAccountStatement(@PathVariable String accountNumber){
        return new ResponseEntity<>(accountService.getAccountStatement(accountNumber), HttpStatus.OK);
    }

    @PostMapping("/deposit")
    public ResponseEntity<?> depositMoney(@RequestBody DepositRequest depositRequest){
        return new ResponseEntity<>(accountService.depositMoney(depositRequest), HttpStatus.OK);
    }

    @PostMapping("/withdrawal")
    public ResponseEntity<?> withdrawMoney(@RequestBody WithdrawalRequest withdrawalRequest){
        return new ResponseEntity<>(accountService.withdrawMoney(withdrawalRequest), HttpStatus.OK);
    }
}
