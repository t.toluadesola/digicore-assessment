package com.digicoreassessment.controllers;

import com.digicoreassessment.payload.request.LoginRequest;
import com.digicoreassessment.payload.request.RegisterAccount;
import com.digicoreassessment.payload.response.LoginResponse;
import com.digicoreassessment.security.service.UserDetailService;
import com.digicoreassessment.services.AccountService;
import com.digicoreassessment.utils.JWTUtil;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class AuthController {

    private final AccountService accountService;



    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserDetailService userDetailService;

    @PostMapping("/create_account")
    public ResponseEntity<?> createAccount(@Valid @RequestBody RegisterAccount registerAccount) {
        return new ResponseEntity<>(accountService.createAccount(registerAccount), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> doLogin(@Valid @RequestBody LoginRequest loginRequest){
        return new ResponseEntity<>(accountService.login(loginRequest, authenticationManager, userDetailService), HttpStatus.OK);
    }


}
