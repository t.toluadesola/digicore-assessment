package com.digicoreassessment.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WithdrawalRequest {

    private String accountNumber;

    private String accountPassword;

    private Double withdrawnAmount;
}
