package com.digicoreassessment.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DepositRequest {
    private String accountNumber;
    private Double amount;
}
