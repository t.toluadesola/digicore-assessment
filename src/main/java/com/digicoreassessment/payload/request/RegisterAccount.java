package com.digicoreassessment.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegisterAccount {
    private String accountName;
    private String accountPassword;
    private Double initialDeposit;
}
