package com.digicoreassessment.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class AccountStatement {

    private LocalDateTime transactionDate;

    private String transactionType;

    private String narration;

    private Double amount;

    private Double accountBalance;
}
