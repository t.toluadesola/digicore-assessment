package com.digicoreassessment.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountResponse {
    private int responseCode;
    private boolean successful;
    private String message;
}
