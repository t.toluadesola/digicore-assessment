package com.digicoreassessment.payload.response;

import com.digicoreassessment.models.Account;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountInfo {

    private int responseCode;

    private boolean success;

    private String message;

    private Account account;

}
