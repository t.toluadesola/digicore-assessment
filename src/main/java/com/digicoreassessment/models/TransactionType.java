package com.digicoreassessment.models;


public enum TransactionType {
    DEPOSIT,
    WITHDRAWAL
}
