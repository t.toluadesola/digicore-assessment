package com.digicoreassessment.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Account {
    private String accountName;
    private String AccountNumber;
    private String accountPassword;
    private Double balance;
}
