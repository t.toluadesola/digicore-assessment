package com.digicoreassessment.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Transactions {
    private String accountNumber;
    private AccountStatement accountStatement;
}
