package com.digicoreassessment.models;

import com.digicoreassessment.models.Account;
import com.digicoreassessment.models.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class AccountStatement {

    private LocalDateTime transactionDate;

    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    private String narration;

    private Double amount;

    private Double accountBalance;
}