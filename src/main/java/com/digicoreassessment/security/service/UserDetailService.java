package com.digicoreassessment.security.service;


import com.digicoreassessment.models.Account;
import com.digicoreassessment.services.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class UserDetailService implements UserDetailsService {

    private final AccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String accountNumber) {
        Account account = accountService.findByAccountNumber(accountNumber);

        return UserDetailsImpl.build(account);
    }
}
