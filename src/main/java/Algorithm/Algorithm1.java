package Algorithm;

public class Algorithm1{

    public static void main(String[] args) {
        System.out.println(doBasicOppOnString("2", "2", "+"));
    }

    public static int doBasicOppOnString(String x, String y, String sign){
        int result = 0;
        switch (sign) {
            case "+" :
                result = convertStringToInt(x) + convertStringToInt(y);
            break;
            case "-" :
                result = convertStringToInt(x) - convertStringToInt(y);
            break;
            case "*" :
                result = convertStringToInt(x) * convertStringToInt(y);
            break;
            case "/" :
                result = convertStringToInt(x) / convertStringToInt(y);
            break;
            default:
                return 0;
        }
        return result;
    }

    public static int convertStringToInt(String num){
        char numToChar = num.charAt(0);
        return (int)numToChar - 48;
    }

}