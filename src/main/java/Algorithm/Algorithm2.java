package Algorithm;

import java.util.Arrays;

public class Algorithm2{

    public static void main(String[] args) {
        int[] score1 = {1, 4, 7, 2, 4};
        int[] score2 = {3, 4, 2, 4, 4};
        System.out.println(Arrays.toString(checkScores(score1, score2)));
    }

    public static int[] checkScores(int[] score1, int[] score2){
        int player1 = 0;
        int player2 = 0;
        if(score1.length == score2.length){
            for(int i = 0; i < score1.length; i++){
                if(score1[i] > score2[i]){
                    player1++;
                }
                if(score1[i] < score2[i]){
                    player2++;
                }
            }
        }

        return new int[]{player1, player2};
    }


}